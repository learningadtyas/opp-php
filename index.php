<?php

//release0
require_once 'animal.php';

$sheep = new Animal("shaun");

echo $sheep->getName(); // "shaun"
echo $sheep->getLegs(); // 4
echo $sheep->getColdBlooded(); // "no"


//release1
require_once 'Animal.php';
require_once 'Frog.php';
require_once 'Ape.php';

$shaun = new Animal('shaun');
echo "Name: " . $shaun->getName() . "\n";
echo "Legs: " . $shaun->getLegs() . "\n";
echo "Cold Blooded: " . $shaun->getColdBlooded() . "\n";

echo "\n";

$buduk = new Frog('buduk');
echo "Name: " . $buduk->getName() . "\n";
echo "Legs: " . $buduk->getLegs() . "\n";
echo "Cold Blooded: " . $buduk->getColdBlooded() . "\n";
$buduk->jump();

echo "\n";

$keraSakti = new Ape('kera sakti');
echo "Name: " . $keraSakti->getName() . "\n";
echo "Legs: " . $keraSakti->getLegs() . "\n";
echo "Cold Blooded: " . $keraSakti->getColdBlooded() . "\n";
$keraSakti->yell();
